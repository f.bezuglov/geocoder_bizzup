<?php

$apiKey = 'xxx';
$address = 'Чапаева, 27';


function geocode_coord_by_address($apiKey, $address, $city='Краснодар') {
    $baseUrl = "https://catalog.api.2gis.com/3.0/items/geocode";
    $q = $city . ', ' . $address;
    $params = [
        'q' => $q,
        'fields' => 'items.point,items.geometry.centroid',
        'key' => $apiKey,
        'format' => 'json',
    ];
    $url = $baseUrl . '?' . http_build_query($params);

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    
    $response = curl_exec($ch);
    curl_close($ch);

    if ($response !== false) {
        $result = json_decode($response, true);
        $lat = $result['result']['items'][0]['point']['lat'];
        $lon = $result['result']['items'][0]['point']['lon'];
        return ['lat' => $lat, 'lon' => $lon]; 
    } else {
        return null;
    }
};

function geocode_district_by_coord($apiKey, $lon, $lat) {
    $baseUrl = "https://catalog.api.2gis.com/3.0/items/geocode";
    $params = [
        'lon' => $lon,
        'lat' => $lat,
        'type' => 'adm_div.district',
        'key' => $apiKey,
        'format' => 'json',
    ];
    $url = $baseUrl . '?' . http_build_query($params);

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    
    $response = curl_exec($ch);
    curl_close($ch);

    if ($response !== false) {
        $result = json_decode($response, true);
        if (isset($result['result']['items'][0]['full_name'])) {
            return $result['result']['items'][0]['full_name'];
        } else {
            return null;
        }
    } else {
        return null;
    }
};

function geocode_district_by_address($apiKey, $address) {
    $coordArray = geocode_coord_by_address($apiKey, $address);
    $district = geocode_district_by_coord($apiKey, $coordArray['lon'], $coordArray['lat']);
    return $district;
};

$result = geocode_district_by_address($apiKey, $address);
var_dump($result);