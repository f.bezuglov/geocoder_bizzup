<?php

$apiKey = 'xxx';
$address = 'Крестьянский переулок, 20';

function geocode($apiKey, $address, $results = 50) {
    $baseUrl = "https://geocode-maps.yandex.ru/1.x/";
    $params = [
        'apikey' => $apiKey,
        'geocode' => $address,
        'results' => $results,
        'format' => 'json'
    ];

    $url = $baseUrl . '?' . http_build_query($params);

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    
    $response = curl_exec($ch);
    curl_close($ch);

    if ($response !== false) {
        return json_decode($response, true);
    } else {
        return null;
    }
}

function getDistrictName($searchResults, $locality="Краснодар") {

    foreach ($searchResults['response']['GeoObjectCollection']['featureMember'] as $featureMember) {
        $geoObject = $featureMember['GeoObject'];
        
        // Проверка на город - Краснодар
        $localityComponent = getComponent($geoObject, 'locality');
        if ($localityComponent['name'] === $locality) {
            // Получение названия района
            $districtComponent = getComponent($geoObject, 'district');
            return $districtComponent['name'] ?? "Not Found";
        }
    }
    return "Not Found";
}

function getComponent($geoObject, $kind) {
    foreach ($geoObject['metaDataProperty']['GeocoderMetaData']['Address']['Components'] as $component) {
        if ($component['kind'] === $kind) {
            return $component;
        }
    }
    return null;
}


$result = geocode($apiKey, $address);
$district = getDistrictName($result);
echo "Район: " . $district;
// var_dump($result);
